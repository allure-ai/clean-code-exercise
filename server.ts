import express from "express";
import bodyParser from "body-parser";
import fs from "fs";
const app = express();
app.use(bodyParser.json());
const PORT = 8000;
type Music = {
  id: number;
  title: string;
  artists: string[];
};
type Book = {
  id: number;
  title: string;
  authors: string[];
};
app.get("/", (req, res) => res.send("Express + TypeScript Server"));
app.get("/music", (req, res) => {
  const music = JSON.parse(fs.readFileSync("music.json").toString());
  res.send(music);
});
app.post("/music", (req, res) => {
  const music = JSON.parse(fs.readFileSync("music.json").toString());
  const id = Math.max(...music.map((m: Music) => m.id)) + 1;

  const newMusic: Music = {
    id,
    title: req.body.title,
    artists: req.body.artists.split(",").map((n: string) => n.trim()),
  };
  fs.writeFileSync("music.json", JSON.stringify([...music, newMusic], null, 4));
  res.send([...music, newMusic]);
});
app.get("/book", (req, res) => {
  const book = JSON.parse(fs.readFileSync("book.json").toString());
  res.send(book);
});
app.post("/book", (req, res) => {
  const book = JSON.parse(fs.readFileSync("book.json").toString());
  const id = Math.max(...book.map((m: Book) => m.id)) + 1;

  const newBook: Book = {
    id,
    title: req.body.title,
    authors: req.body.authors.split(",").map((n: string) => n.trim()),
  };
  fs.writeFileSync("book.json", JSON.stringify([...book, newBook], null, 4));
  res.send([...book, newBook]);
});
app.listen(PORT, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${PORT}`);
});
